export type Order = {
  order_details: OrderDetail[]
}

type OrderDetail = {
  id: number
  quantity: number
  image: string
  product: Product
  price: string
  product_id: string
}

type Product = {
  id: string
  in_stock: number
  passenger_capacity: number
  title: string
  maximum_speed: number
}

export const getOrder = async (id: number) => {
  const response = await fetch(`http://localhost:3000/orders/${id}`, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
  const data = (await response.json()) as Order
  return data
}
