/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		colors: {
			'dark-gray': '#35344C',
			'medium-gray': '#474374',
			'light-gray': '#C3C1D7',
			white: '#FFF',
			'dark-violet': '#3F25C8',
			violet: '#745CF1',
			'light-violet': '#F3F1FE',
			gray: '#E5E5E4',
			'bg-gray': '#FCFCFD',
		},
		fontFamily: {
			sans: ['Amplitude', 'sans-serif'],
			secondary: ['Inter', 'sans-serif'],
		},
		extend: {
			spacing: {
				17.5: '4.375rem',
				82: '20.5rem',
			},
			lineHeight: {
				tighter: '1.168rem',
			},
		},
	},
	plugins: [],
}
