"""init

Revision ID: d459f6716b9d
Revises: f2b1adc5ca27
Create Date: 2023-04-10 17:43:48.138683

"""

# revision identifiers, used by Alembic.
revision = 'd459f6716b9d'
down_revision = 'f2b1adc5ca27'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
